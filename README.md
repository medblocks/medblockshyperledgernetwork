# ClientNodes

Client-side nodes for our network

##Topology
orderer.medblocks.com
peer0.oracle.medblocks.com
peer0.client.medblocks.com
peer1.client.medblocks.com

the oracle peer is meant to be used to create users. No other nodes should be able to create users.
Either of the client peers can be used for the other functions.

Only a CLI node runs locally which can be used to manipulate the network on the server

## Getting Started

Follow These instructions to get your client runnning. A static server IP is needed to connect to the server.

### Prerequisites

Same Pre-requisites as hyperledger needed on the client

```
https://hyperledger-fabric.readthedocs.io/en/release-1.4/build_network.html#install-prerequisites
```

### Usage

The start.sh script starts the cli node with environment variables bootstrapped to peer0.oracle.medblocks.com

```
./start.sh
```


The stop.sh script stops the cli node

```
./stop.sh
```

##NOTE

TLS is disabled. Queries that include TLS won't work.

If using a different server, modify the start.sh script to reflect your server's IP ADDRESS
```
export SERVERADDR=IPADDRESS
```

All requests must be made to $SERVERADDR:{port}

The following replacements must be made to the demo commands to run it with our network
```
example.com -> medblocks.com
org1 -> oracle
Org1 -> Oracle
org2 -> client
Org2 -> Client
```

###Example Commands

Channel create
```
export CHANNEL_NAME=mychannel
peer channel create -o orderer.example.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
```
changes to 
```
export CHANNEL_NAME=mychannel
peer channel create -o $SERVERADDR:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx
```
Removed TLS files and changed endpoint for request

join peer0.org2
```
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp CORE_PEER_ADDRESS=peer0.org2.example.com:9051 CORE_PEER_LOCALMSPID="Org2MSP" CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt peer channel join -b mychannel.block
```
changes to
```
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/client.medblocks.com/users/Admin@client.medblocks.com/msp CORE_PEER_ADDRESS=peer0.client.medblocks.com:9051 CORE_PEER_LOCALMSPID="ClientMSP" peer channel join -b mychannel.block
```
Removed TLS files, changed org2 to client, changed example to medblocks

update anchor peers
```
peer channel update -o orderer.example.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/Org1MSPanchors.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
```
changes to
```
peer channel update -o $SERVERADDR:7050 -c $CHANNEL_NAME -f ./channel-artifacts/OracleMSPanchors.tx
```
Removed TLS files, changed org1 to oracle, chenged endpoint for request


