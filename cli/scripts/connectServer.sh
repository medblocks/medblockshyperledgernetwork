#!/bin/bash
export CHANNEL_NAME=mychannel
export SERVERADDR=$1

CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/client.medblocks.com/users/Admin@client.medblocks.com/msp CORE_PEER_ADDRESS=peer1.client.medblocks.com:10051 CORE_PEER_LOCALMSPID="ClientMSP" peer channel join -b mychannel.block
